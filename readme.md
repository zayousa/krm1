# KCRM

## Setup

- install composer `composer install`
- install node packages `yarn install` or `sudo npm install`

## Add coffeeScript compile in Gulp
 - mix.coffee('app.coffee');

## Setup back up
- edit file `config/database.php`
  - change `dump_command_path` value to `'dump_command_path' => '/usr/bin'` (for Linux)

## Install dependance
- edit file `composer.json`
```
"require": {
...
        "predis/predis": "^1.1",
        "illuminate/support": "5.*",
        "laravelcollective/html": "^5.2|^5.1",
        "illuminate/container": "^5.2|^5.1",
        "yajra/laravel-datatables-oracle": "~6.0",
        "creativeorange/gravatar": "^1.0",
        "doctrine/dbal": "~2.3",
        "zizaco/entrust": "1.7.0",
        "spatie/laravel-backup": "^3.0.0"
...
},
```

- puis launch `composer update`

## Add Providers & Alias
- edit file `config/app.php`:
```php
'providers' => [
...
        Collective\Html\HtmlServiceProvider::class,
        Yajra\Datatables\DatatablesServiceProvider::class,
        Creativeorange\Gravatar\GravatarServiceProvider::class,
        Zizaco\Entrust\EntrustServiceProvider::class,
        Spatie\Backup\BackupServiceProvider::class
...
],
```


```php
'aliasses' => [
...
        'Form' => Collective\Html\FormFacade::class,
        'HTML' => Collective\Html\HtmlFacade::class,
        'Gravatar' => Creativeorange\Gravatar\Facades\Gravatar::class,
        'CodeGenerator' => CodeGenerator::class,
        'LAFormMaker' => LAFormMaker::class,
        'LAHelper' => LAHelper::class,
        'Module' => App\Module::class,
        'LAConfigs' => App\LAConfigs::class,

        // For Entrust
        'Entrust' => Zizaco\Entrust\EntrustFacade::class,
        'role' => Zizaco\Entrust\Middleware\EntrustRole::class,
        'permission' => Zizaco\Entrust\Middleware\EntrustPermission::class,
        'ability' => Zizaco\Entrust\Middleware\EntrustAbility::class
...
],
```