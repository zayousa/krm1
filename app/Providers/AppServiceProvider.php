<?php

namespace App\Providers;

use Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use LAHelper;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // LAForm Input Maker
        Blade::directive('la_input', function($expression) {
            if(LAHelper::laravel_ver() == 5.3) {
                $expression = "(".$expression.")";
            }
            return "<?php echo LAFormMaker::input$expression; ?>";
        });

        // LAForm Form Maker
        Blade::directive('la_form', function($expression) {
            if(LAHelper::laravel_ver() == 5.3) {
                $expression = "(".$expression.")";
            }
            return "<?php echo LAFormMaker::form$expression; ?>";
        });

        // LAForm Maker - Display Values
        Blade::directive('la_display', function($expression) {
            if(LAHelper::laravel_ver() == 5.3) {
                $expression = "(".$expression.")";
            }
            return "<?php echo LAFormMaker::display$expression; ?>";
        });

        // LAForm Maker - Check Whether User has Module Access
        Blade::directive('la_access', function($expression) {
            if(LAHelper::laravel_ver() == 5.3) {
                $expression = "(".$expression.")";
            }
            return "<?php if(LAFormMaker::la_access$expression) { ?>";
        });
        Blade::directive('endla_access', function($expression) {
            return "<?php } ?>";
        });

        // LAForm Maker - Check Whether User has Module Field Access
        Blade::directive('la_field_access', function($expression) {
            if(LAHelper::laravel_ver() == 5.3) {
                $expression = "(".$expression.")";
            }
            return "<?php if(LAFormMaker::la_field_access$expression) { ?>";
        });
        Blade::directive('endla_field_access', function($expression) {
            return "<?php } ?>";
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //tada ;)
    }
}
