<?php
/**
 * Created by PhpStorm.
 * User: karim
 * Date: 20/07/17
 * Time: 14:40
 */


/**
 * Translation method
 * @param $str
 * @return string|\Symfony\Component\Translation\TranslatorInterface
 */
function t($str, $file='app'){
    return trans("$file.$str");
}

/**
 * @param $module
 * @param $attr
 * @return mixed
 */
function attr($module, $attr){
    if(!property_exists($module, $attr)){
        return false;
    }
    return true;
}

function getAttr($module, $attr){
    if(!property_exists($module, $attr)){
        return null;
    }
    return $module->row->{$attr};
}

/**
 * @param Model $master
 * @param Model $slave
 * @return bool
 */
function addRelation(Model $master, Model $slave){

    return true;
}