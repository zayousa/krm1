<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* ================== Homepage + Admin Routes ================== */

$as = "";
if(App\Http\Helpers\LAHelper::laravel_ver() == 5.3) {
    $as = config('laraadmin.adminRoute').'.';

    // Routes for Laravel 5.3
    Route::get('/logout', 'Auth\LoginController@logout');
}


require __DIR__.'/admin_routes.php';
require __DIR__.'/modules_routes.php';