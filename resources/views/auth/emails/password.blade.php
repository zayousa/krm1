{{-- resources/views/emails/password.blade.php --}}
 
{{ t('reset_link') }}: <a href="{{ url('password/reset/'.$token) }}">{{ url('password/reset/'.$token) }}</a>