{{ t('dear') }} {{ $user->name }},<br><br>

{{ t('credentials_changed') }}:<br><br>

{{ t('username') }}: {{ $user->email }}<br>
{{ t('password') }}: {{ $password }}<br><br>

{{ t('u_login') }} <a href="{{ url('/login') }}">{{ str_replace("http://", "", url('/login')) }}</a>.<br><br>

{{ t('best_regards') }},