@if(!isset($no_padding))
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        {{ t('powered') }} <a href="http://karimoc.com/">Zayousa</a>
    </div>
    <strong>{{ t('copyright') }} 2017 @if(Carbon\Carbon::now()->format('Y') != 2017) - Carbon\Carbon::now()->format('Y') @endif
</footer>
@endif